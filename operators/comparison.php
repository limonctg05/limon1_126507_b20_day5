<?php
$x = 100;
$y = "100";
var_dump($x == $y); // returns true because values are equal
echo "<hr>";
?>

<?php
$x = 100;
$y = "100";
var_dump($x == $y); // returns true because values are equal
echo "<hr>";
?>

<?php
$x = 100;
$y = "100";
var_dump($x != $y); // returns false because values are equal
echo "<hr>";
?>

<?php
$x = 100;
$y = "100";
var_dump($x <> $y); // returns false because values are equal
echo "<hr>";
?>

<?php
$x = 100;
$y = "100";
var_dump($x !== $y); // returns true because types are not equal
echo "<hr>";
?>

<?php
$x = 100;
$y = 50;
var_dump($x > $y); // returns true because $x is greater than $y
echo "<hr>";
?>

<?php
$x = 10;
$y = 50;
var_dump($x < $y); // returns true because $x is less than $y
echo "<hr>";
?>

<?php
$x = 50;
$y = 50;
var_dump($x >= $y); // returns true because $x is greater than or equal to $y
echo "<hr>";
?>

<?php
$x = 50;
$y = 50;
var_dump($x <= $y); // returns true because $x is less than or equal to $y
?>



